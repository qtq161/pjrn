const assert = require('assert')

const pjrn = require('../dist/pjrn')

assert.deepStrictEqual(pjrn('{"a": 1}'), {a:1}, 'Valid JSON is parsed')
assert.strictEqual(pjrn('<'), null, 'Invalid JSON returns null')
